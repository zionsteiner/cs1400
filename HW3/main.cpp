//Final Grade Calc
/* Zion Steiner
    A02245003
    CS1400 Section 001
*/

//program header
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;
int main()
{
    //sets up string object "fin" for input file
    ifstream fin("scores.txt");     
    //defines all scores as floats
    float hw1Score, hw2Score, hw3Score, hw4Score, hw5Score, hw6Score, hw7Score, hw8Score, hw9Score, hw10Score;  
    float exam1Score, exam2Score;
    float recitationScore;
    //sets total possible scores for each category to their respective integer values
    const int totalPosHWScores=100, totalPosExamScores=250, totalPosRecitationScores=50, totalPosPoints=400;
    //this statement will read all the data in "scores.txt" and store it in the proper variables
    fin >> hw1Score >> hw2Score >> hw3Score >> hw4Score >> hw5Score >> hw6Score >> hw7Score >> hw8Score >> hw9Score >> hw10Score        
        >> exam1Score >> exam2Score >> recitationScore;
    fin.close();        //closes file with the ifstream file close member function
    //defines percentage variables for each category    
    float hwPercentage = (hw1Score + hw2Score + hw3Score + hw4Score + hw5Score + hw6Score + hw7Score + hw8Score + hw9Score + hw10Score)/totalPosHWScores,
          examPercentage = (exam1Score + exam2Score)/totalPosExamScores,
          recitationPercentage = recitationScore/totalPosRecitationScores;
    //prints each category's percentage grade separately     
    cout << "Your scores are being calculated from the selected file.\n"
         << "Please wait...\n"
         << fixed << setprecision(1) << showpoint
         << "Homework grade: " << hwPercentage*100 << "%\n"
         << "Exam grade: " << examPercentage*100 << "%\n"
         << "Recitation grade: " << recitationPercentage*100 << "%" << endl;
    //defines final percentage variable
    float finalPercentage = ((hwPercentage + examPercentage + recitationPercentage)/3)*100;
    cout << "Your final percentage is: " << finalPercentage << "%" << endl;
    //if/else if control structure for displaying the proper message for each grade. The message printed is selected based on what the final percentage ended up to be.
    if (finalPercentage>=94)
        cout << "Your final grade is an A! You really know your stuff!";
    else if (finalPercentage>=84)
        cout << "In the end, it'll all B alright. You finished with a B.\n A little more effort, and you'll get an A next time!";
    else if (finalPercentage>=74)
        cout << "C's get degrees; you passed! You got a C; try not to cut it so close next time!";
    else if (finalPercentage>=64)
        cout << "Your grade is: D. Not very exciting, is it?";
    else
        cout << "Final grade: F. You failed. That's it. To play again, please insert next semester's tuition." << endl;
    return 0;
}
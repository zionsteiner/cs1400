//Deck class definition

#ifndef DECK_HPP
#define DECK_HPP
#include "PlayingCard.hpp"

class Deck
{
    private:
        static const int numCards=52;        //Deck array size declarator
        static const int numValues=13;       //For deck initialization
        static const int numSuites=4;        //For deck initialization
        PlayingCard deck[numCards];
        int value[numValues]={2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11};              //Array of card values for deck initialization
        std::string face[numValues]={"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};       //Array of face values for deck initialization
        std::string suit[numSuites]={"Clubs", "Diamonds", "Hearts", "Spades"};         //Array of suit values for deck initialization
    public:
        Deck();                     //Initializes deck array
        void shuffle();             //Shuffles the deck
        PlayingCard draw();         //Returns the card at the top of the deck
        void printDeck();           //Prints deck in draw order. Use for testing
};

#endif
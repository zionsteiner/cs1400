//Blackjack class implementation
#include <iostream>
#include "Blackjack.hpp"

//Shuffles and sets scores to 0
Blackjack::Blackjack()
{
    spidermanDeck.shuffle();
    playerScore=0;
    dealerScore=0;
}

//Processes player turn
void Blackjack::playerTurn()
{
    char choice;
    bool quit=false;
    
    while(!quit)
    {
        std::cout<<"You have "<<playerScore<<" points"<<std::endl;
        
        std::cout<<"Enter h to Hit or s to Stand: ";
        std::cin>>choice;
        
        //Input validation
        while(choice!='H' && choice!='h' && choice!='S' && choice!='s')
        {
            std::cout<<"Please enter a valid option"<<std::endl;
            std::cin>>choice;
        }
        
        if (choice=='H'||choice=='h')   //Hit draws another card and adds it to playerScore
        {
            PlayingCard playerCard3=spidermanDeck.draw();
            playerScore+=playerCard3.getValue();
            
            std::cout<<"You drew the "<<playerCard3.toString()<<std::endl;
        }
        else if (choice=='S'||choice=='s')  //Stand ends player's turn
        {
            std::cout<<"Player stands. Dealer's turn"<<std::endl;
            quit=true;
        }
        
        //Checks for bust
        if (playerScore>21)
        {
            std::cout<<"You bust!"<<std::endl;
            quit=true;
        }
    }
}

//Processes dealer turn
void Blackjack::dealerTurn(PlayingCard card2)
{
    bool bust=false;
    
    std::cout<<"Dealer's face down card was a "<<card2.toString()<<std::endl; 
    dealerScore+=card2.getValue();
    
    if(dealerScore>21)
    {
        bust=true;
        if(dealerScore>21)
        {
            std::cout<<"Dealer busts!"<<std::endl;
            std::cout<<std::endl;
        }    
    }
    
    //Only runs while the dealer's score is under 17 and the dealer has not bust
    while(dealerScore<17 && !bust)
    {
        PlayingCard dealerCard3=spidermanDeck.draw();
        std::cout<<"Dealer draws a "<<dealerCard3.toString()<<std::endl;
        dealerScore+=dealerCard3.getValue();
        if(dealerScore>21)
        {
            bust=true;
            if(dealerScore>21)
            {
                std::cout<<"Dealer busts!"<<std::endl;
                std::cout<<std::endl;
            }    
        }
    }
}

//Runs a game of blackjack
void Blackjack::play()
{
    bool blackjack=false;
    
    //Draws, displays, and adds the points of the player's cards
    PlayingCard playerCard1=spidermanDeck.draw();
    playerScore+=playerCard1.getValue();    
    PlayingCard playerCard2=spidermanDeck.draw();
    playerScore+=playerCard2.getValue();
    std::cout<<"You are dealt the "<<playerCard1.toString()<<" and the "<<playerCard2.toString()<<std::endl;
    
    //Draws, displays, and adds the points of the dealer's showing card
    PlayingCard dealerCard1=spidermanDeck.draw();
    dealerScore+=dealerCard1.getValue();
    PlayingCard dealerCard2=spidermanDeck.draw();
    std::cout<<"The dealer is showing the "<<dealerCard1.toString()<<std::endl;
    
    //Player's turn starts unless they got a blackjack
    if (playerScore==21)
        blackjack=true;
    else    
        playerTurn();
    
    //Dealer's turn starts unless player bust or got a blackjack
    if (playerScore<=21 && !blackjack)
        dealerTurn(dealerCard2);
        
    //Displays score and determines winner
    std::cout<<"Final Score\n"
             <<"_____________\n"
             <<"Your Score: "<<playerScore<<"\n"
             <<"Dealer's Score: "<<dealerScore<<std::endl;
             
    //Tie condition
    if(playerScore==dealerScore)
    {
        std::cout<<"Tie!"<<std::endl;
        std::cout<<std::endl;
    }
    
    //Dealer win conditions         
    else if((playerScore<dealerScore && dealerScore<=21)||playerScore>21)
    {
        std::cout<<"Dealer wins"<<std::endl;
        std::cout<<std::endl;
    }
    //Player win conditions
    else if((playerScore>dealerScore && playerScore<=21)||dealerScore>21)
        std::cout<<"You win! Good job!"<<std::endl;
        std::cout<<std::endl;
}
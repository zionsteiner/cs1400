//PlayingCard class implementation

#include <iostream>
#include "PlayingCard.hpp"

using namespace std;

PlayingCard::PlayingCard()
{
    value=0;
    face="0";
    suit="None";
}

PlayingCard::PlayingCard(int cardValue, std::string cardFace, std::string cardSuit)   
{
    value=cardValue;
    face=cardFace;
    suit=cardSuit;
}

std::string PlayingCard::toString()
{
    return face + " of " + suit;
}

int PlayingCard::getValue()
{
    return value;
}

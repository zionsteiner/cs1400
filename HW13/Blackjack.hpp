//Blackjack class definition
#ifndef BLACKJACK_HPP
#define BLACKJACK_HPP
#include "Deck.hpp"

class Blackjack
{
    private:
        Deck spidermanDeck;
        int playerScore;
        int dealerScore;
        void playerTurn();      //Processes player turn
        void dealerTurn(PlayingCard);      //Processes dealer turn
    public:
        Blackjack();    //Shuffles deck and sets scores to 0;
        void play();    //Runs a game of blackjack
};

#endif
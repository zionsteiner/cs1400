//Deck class implementation

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Deck.hpp"

//Initializes deck array with 52 cards
Deck::Deck()
{
    int index=0;
    
    for(int i=0; i<numValues; i++)
    {
        for(int j=0; j<numSuites; j++)
        {
            deck[index]=PlayingCard(value[i], face[i], suit[j]);
            index++;
        }
    }
}

//Shuffles and "refills" the deck
void Deck::shuffle()
{
    int randIndex;    //To hold randomly generated index  
    PlayingCard tempVar;      //To store card during switch 
    srand(time(0));
    
    //Reintializes and "refills" deck
    Deck();
    
    //Switches card deck[i] with another random card
    for (int i=0; i<numCards; i++) {
        randIndex=rand()%numCards;
        tempVar=deck[i];        
        deck[i]=deck[randIndex];  
        deck[randIndex]=tempVar;  
    }
}

//Returns the card at the top o the deck
PlayingCard Deck::draw()
{
    PlayingCard tempCard=deck[0]; //Saves the first card of the deck before card shift
    for (int i=0; i<51; i++)     //Shifts deck position up by one, except the last card, effectively removing the first card after it is "drawn"
        deck[i]=deck[i+1];  
    deck[51]=PlayingCard();     //After shift, reinitializes card 52 in deck as having no suit, with a face and value of 0
    return tempCard;            //Returns the first card of the deck
}

//Prints deck in draw order. Use for testing
void Deck::printDeck()
{
    std::cout<<"Printing deck..."<<std::endl;
    
    for(int i=0; i<52; i++)
        std::cout<<deck[i].toString()<<std::endl;
    std::cout<<std::endl;
}
//PlayingCard class definition

#ifndef PLAYINGCARD_HPP
#define PLAYINGCARD_HPP

class PlayingCard
{
    private:
        int value;           //1, 2, 3, etc.
        std::string face;    //1, 2, 3, King, Queen, etc.
        std::string suit;    //Ace, spades, etc.
    public:
        PlayingCard();                                          //Default constructor sets face to "0", value to 0, and suit to none
        PlayingCard(int, std::string, std::string);             //Parameterized constructor
        std::string toString();                                 //Returns a string card representation
        int getValue();                                         
        std::string getFace();
        std::string getSuit();
}; 

#endif
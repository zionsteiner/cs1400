#include <iostream>
#include "Blackjack.hpp"

int main()
{
    char choice;
    
    //Plays game of blackjack and prompts user to play again
    do
    {
        Blackjack palazzoFloor;
        palazzoFloor.play();
        std::cout<<"Would you like to play again?\n"
                 <<"Enter Y to play again, and anything else to quit."<<std::endl;
        std::cin>>choice;
        std::cout<<std::endl;
    }    
    while(choice=='Y'||choice=='y');
    
    return 0;
}
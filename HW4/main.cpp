/*Egg Drop Program HW4
Zion Steiner
A02245003
CS1400 Section 001*/
//program framework
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

//function prototypes for GetT, GetD, Report
float GetT(float, float);
float GetD(float, float);
void Report(int, float, float, float, float);

//****************************
//function definition of main*
//****************************
int main()
{
    //variable definitions for velocity and height
    float v, h;
    
    //variable "line" is used here to keep track of what line the data is read from
    int line = 0;
    
    //file "input.txt" is linked to input file stream object
    fstream fin("input.txt");
    
    /*This while-loop terminates when the program stops receiving data from the file.
    Integer "line" is incremented every loop to keep track of what line what data came from.
    Next, velocity and height are read from the file and are assigned to their variables.
    The function report is called to display line number, v, h, d, and t.*/
    while (!fin.eof())
    {
        line++;
        fin >> v >> h;
        
        //Functions GetD and GetT are called to retrieve arguments for D and T
        Report(line, v, h, GetD(v, h), GetT(v, h));
    }
    
    //closes file "input.txt"
    fin.close();
    
    return 0;
}

/*This function formats the calculated information in a readable format. 
It is designed to accept 5 variables. It doesn't need to return anything.*/
void Report(int line, float v,   float h,   float d,   float t)
{
    cout <<   "On line "  <<   right <<   setw(2) <<   line
         << ",  v =  "  <<   setw(3) << v << ",  h =  "  <<   setw(5)
         << h  <<   setprecision(5)<< ",  d =  "  <<   setw(6) << d  <<   "m"
         << ",  t =  "  <<   setw(6) << t  <<   "s" <<   endl;
}

/*GetD uses an if/else statement to return 0 if the denominator is zero. 
Otherwise, it returns the calculated value of distance to target*/
float GetD(float v, float h)
{
    //gravity only needs to be define here since it is only used in this function
    const float g = 9.8;
    float d = v*sqrt(2.0*(h/g));
    if (g == 0)
        return 0.0;
    else
    {
        return d;
    }
}

/*GetD uses an if/else statement for the same reason.
It returns the calculated value of time to impact.*/
float GetT(float v, float h)
{
    float t = GetD(v, h)/v;
    if (v == 0)
        return 0.0;
    else
    {
        return t;
    }
}
f = open('prices/aapl.txt', 'r')
lines = f.read().splitlines()

prcs=[]
for line in lines: prcs.append(float(line))


lowRange=3
highRange=25

buysArr=[]
sellsArr=[]
profitArr=[]
annReturnsArr=[]
buyHoldRetArr=[]

for x in range(lowRange, highRange+1):
    i=0
    buy=0.0
    profit=0.0
    buys=0
    sells=0
    mvaX=0.0
    for p in prcs:
        if(i>x):
            prcsSum=0.0
            for k in range(1, x+1):
                prcsSum+=prcs[i-k]
            
            mvaX=prcsSum/x
            if(p>mvaX and buy==0.0):
                buy=p
                buys+=1
            elif(p<mvaX and buy!=0.0):
                profit+=(p-buy)
                buy=0.0
                sells+=1
        i+=1
    
    buysArr.append(buys)
    sellsArr.append(sells)
    profitArr.append(profit)
    annReturnsArr.append(profit/prcs[0])
    buyHoldRetArr.append(prcs[-1]/prcs[0])
    
maxBuys=max(buysArr)
maxSells=max(sellsArr)
maxProfit=max(profitArr)
maxAnnReturn=max(annReturnsArr)
maxBuyHoldRet=max(buyHoldRetArr)

print("Max Buys:", maxBuys, "on", buysArr.index(maxBuys)+lowRange, "day mva")
print("Max Sells:", maxSells, "on", sellsArr.index(maxSells)+lowRange, "day mva")
print("Max Profit:", maxProfit, "on", profitArr.index(maxProfit)+lowRange, "day mva")
print("Max Annual Returns:", maxAnnReturn, "on", annReturnsArr.index(maxAnnReturn)+lowRange, "day mva")
print("Max Buy/Hold Returns:", maxBuyHoldRet, "on", buyHoldRetArr.index(maxBuyHoldRet)+lowRange, "day mva")
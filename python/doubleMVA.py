f = open('prices/EA.txt', 'r')
lines = f.read().splitlines()

prcs=[]
for line in lines: prcs.append(float(line))

i=0
buy=0.0
profit=0.0
buys=0
sells=0
mva5=0.0
mva20=0.0

for p in prcs:
    if(i>20):
        mva5=(prcs[i-1]+prcs[i-2]+prcs[i-3]+prcs[i-4]+prcs[i-5])/5.0
        prcsSum=0.0
        for x in range(1, 21):
            prcsSum+=prcs[i-x]
        mva20=prcsSum/20
            
        if(mva5>mva20 and buy==0.0):
            buy=p
            buys+=1
            print("Bought at price: ", p)
        elif(mva5<mva20 and buy!=0.0):
            profit+=(p-buy)
            buy=0.0
            print("Sold at price: ",p)
            sells+=1
    i+=1

print ("Num buys: ",buys)
print ("Num sells: ",sells)
print ("Total profit: ",profit)
print ("Annual returns: ",profit/prcs[0])
print ("Buy/Hold returns: ",prcs[-1]/prcs[0])
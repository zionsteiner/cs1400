/*Zion Steiner
  A02245003
  YAHTZEE HW8
  CS 1400 Section 001
*/
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int reRoll(int, int);

int main()
{
    int die1, die2, die3, die4, die5;
    
    //This will be used for repeating or exiting the game if the user wants to play again
    char playAgain;
    
    //seeds rand() based on time
    unsigned int seed=time(0);
    srand(seed);

    bool play=true;
    bool yahtzee=false;
    
    //Title
    cout<<"YAHTZEE"<<endl;
    
    /*This loop runs while bool play is true. When a user wins, they are prompted to play again.
    If enter Y to play again, play remains true and the loop and game runs again. If the user enters anything else, play
    is set to false and the loop terminates, ending the game and program.*/
    while(play)
    {
        //roll# counter
        int i=1;
        
        //Initializes and displays all die as random numbers 1-6.
        die1=rand()%6+1, die2=rand()%6+1, die3=rand()%6+1, die4=rand()%6+1, die5=rand()%6+1;
        cout<<"First Roll: "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        
        /*While yahtzee is false, this loop will ask the user what number they want to hold.
        All die that are not the same as the hold number are assigned another random number 1-6.
        This continues until a yahtzee is reached. When all 5 die are the same number, a 
        congratulatory message is printed and bool yahtzee is set to true.*/
        while(!yahtzee)
        {
            if(die1==die2 && die2==die3 && die3==die4 && die4==die5)
            {
                yahtzee=true;
                cout<<"You gotta YAHTZEE with the number "<<die1<<" in "<<i<<" rolls!!!"<<endl;
                continue;
            }
            i++;
            int holdnum;
            cout<<"Which number would you like to hold?"<<endl;
            cin>>holdnum;
            
            //Validation for hold number
            while(holdnum!=die1 && holdnum!=die2 && holdnum!=die3 && holdnum!=die4 && holdnum!=die5)
            {
                cout<<"You can only hold a number if you already have a die of that number.\nPlease enter a valid number."<<endl;
                cin>>holdnum;
            }
            
            die1=reRoll(die1, holdnum), die2=reRoll(die2, holdnum), die3=reRoll(die3, holdnum), die4=reRoll(die4, holdnum), die5=reRoll(die5, holdnum);
            cout<<"Roll #"<<i<<": "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        }
        //yahtzee=false;
        cout<<"To play again, press Y. Press anything else to quit."<<endl;
        cin>>playAgain;
        if(playAgain=='Y'||playAgain=='y')
            continue;
        else if(playAgain!='Y'||playAgain!='y')
        {
            play=false;
            cout<<"Thanks for playing!"<<endl;
        }
    }
    return 0;
}

/*When called, this function checks the die number. If the number is the same
as the hold number, it passes back the original die number. If not, a new random 
number 1-6 is generated and passed back to that die.*/
int reRoll(int die, int holdnum)
{
    if(die!=holdnum)
        return rand()%6+1;
    else
        return die;
}
/*Caesar Cypher CS1400 Section 001
Zion Steiner
A02245003*/


#include <iostream>
#include <fstream>
using namespace std;

//function prototypes
bool isUpper(char character);
bool isLower(char character);
char rotate(char character, int offset);

int main()
{
    //defines and initialize offset to 0
    int offset=0;
    
    //This loop will cycle 26 times because offset starts at 0. This is 1 time per cypher key.
    while(offset<26)
    {
        //opens file for this cycle
        ifstream fin("messages/mystery.txt");
        
        //prints header
        cout << "=========================\n" 
             << "Rotating by " << offset <<" positions\n"
             << "=========================\n"; 
             
        //Loop cycles until all text from file has been read, printing each decoded character
        while(!fin.eof())
        {
            //prints returned char from the rotate() function
            cout << rotate(fin.get(), offset);
        }
        
        //ends line in preparation for next loop
        cout << endl;
        
        //increments offset variable so the next loop will use 1 as the key
        offset++;
        
        //closes file for this cycle
        fin.close();
    }
    return 0;
}

//This function returns true is the entered char is uppercase, or false for anything else. I used ASCII values to set inequality.
bool isUpper(char character)
{
    if(character>=65 && character<=90)
        return true;
    else return false;
}

//Returns true for lowercase char, or false for anything else
bool isLower(char character)
{
    if(character>=97 && character<=122)
        return true;
    else return false;
}

//When a char and the key are entered, it returns the decoded char according to that key.
char rotate(char character, int offset)
{
    
    //defines complimentary offset
    int compOffset = 26-offset;
    
    //This logic runs if argument is an uppercase letter
    if(isUpper(character))
    {
        //This logic accounts for if char would go outside of its lower or uppercase bounds, and then decodes it.
        if(character+compOffset<=90)
            return character+compOffset;
        else if(character+compOffset>90)
            return character-offset;
    }
    
    //Runs if argument is lowercase letter
    else if(isLower(character))
    {
        if(character+compOffset<=122)
            return character+compOffset;
        else if(character+compOffset>122)
            return character-offset;
    }
    
    //If argument is neither upper or lowercase, it is returned unchanged.
    else return character;
}
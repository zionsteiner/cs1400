//Score Calculator
/* Zion Steiner
    A02245003
    CS1400 Section 001
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int homeworkPts, examPts, recitationPts, totalPtsEarned;    //define variables
    const int totalPosPts = 475;
    float gradePercentage;
    cout << "This program will calculate your grade as a percentage. \n"
         << "Please enter your points earned from homework out of 150. \n"; //hw prompt
    cin >> homeworkPts;                                                     //hw response
    cout << "Please enter your points earned from exams out of 250. \n";    //exam prompt
    cin >> examPts;                                                         //exam response
    cout << "Please enter your points earned from recitations out of 75. \n";   //recitation prompt
    cin >> recitationPts;                                                       //recitation response
    
    totalPtsEarned = (homeworkPts+examPts+recitationPts);                   //calculates total of int variables, then promotes the sum to a float when assigned to totalPtsEarned
    cout << "You have earned " << totalPtsEarned << " out of 475. \n";      //displays points earned out of 475
    
    gradePercentage = (static_cast<float>(totalPtsEarned)/totalPosPts)*100;                     //calculates %
    cout << "Your grade expressed as a percentage is "                      //displays grade % with to 3 sigfigs
         << setprecision(3) << gradePercentage << "%. \n";
         
         if (gradePercentage > 94)                                          //grade-dependent conditionally executed statements
            cout << "Your grade is equivalent to an A. Congratulations!";      //print for A grade
         if (gradePercentage < 61)
            cout << "You failed this course. Try harder next time.";           //print for F grade
         
    return 0;
}
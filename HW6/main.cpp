#include <iostream>
#include <iomanip>
using namespace std;

//function prototypes
int mainMenu();
float insertCash();
int sodaSelection();
void printStatus(int mtnDewInv, int pepsiInv, int aquafinaInv, float cashInserted, float machineBank);

int main()
{
    //variables for inventory
    int mtnDewInv, pepsiInv, aquafinaInv;
    mtnDewInv=pepsiInv=aquafinaInv=5;
    float cashInserted=0.00, machineBank=0.00;
    
    //Most of the function is nested inside a bool controlled do-while loop. When proceed is set to false, the loop will continue
    bool proceed;
    do
    {
        //Sets proceed to true so that loop will terminate on the condition it isn't set to false
        proceed=true;
        
        //Calls function mainMenu and stores the returned menu choice in int selection.
        int selection=mainMenu();
        
        //If else if statements for menu selection functions
        if (selection==1)
        {
            //Calls function insertCash to ask and return how much money the customer wants to spend
            float moneyInserted=insertCash();
            
            //Adds moneyInserted to cashInserted because cashInserted has a function-wide scope
            cashInserted+=moneyInserted;
            
            //Sets proceed to false and then uses continue statement to go back to the beginning of the first do-while loop.
            proceed=false;
            continue;
        }
        
        /*Calls soda menu. If enough money has been entered and the selected drink is available, the drink is sold.
          If not, the customer is asked to add more money or is informed that the machine is out of that selection.*/
        else if (selection==2)
        {
            int soda=sodaSelection();
                if ((soda==1 && mtnDewInv==0)||(soda==2 && pepsiInv==0)||(soda==3 && aquafinaInv==0))
                {
                    cout << endl << "Guess that soda is too popular! We're all out!" << endl;
                    proceed=false;
                    continue;
                }    
                else if (cashInserted<1.50)
                {
                    cout << endl << "Give us the money, and wipe away the debt! This is your last chance!" << endl;
                    proceed=false;
                    continue;
                }    
                else
                {
                    if (soda==1)
                        mtnDewInv--;
                    else if (soda==2)
                        pepsiInv--;
                    else if (soda==3)
                        aquafinaInv--;
                        
                    //Else statement executes and goes back to main menu when soda choice==4, or when cancel was selected
                    else
                    {
                        proceed=false;
                        continue;
                    }
                    
                    //Customer's cash is reduced, and the same amount is added to the machine bank
                    cashInserted-=1.50;
                    machineBank+=1.50;
                    cout << endl << "Thank you for your purchase!" << endl; 
                    proceed=false;
                    continue;
                }
        }
        
        //Calls function to print machine status
        else if (selection==3)
        {
            printStatus(mtnDewInv, pepsiInv, aquafinaInv, cashInserted, machineBank);
            proceed=false;
            continue;
        }
        
        //Restocks machine and removes all money from machine bank
        else if (selection==4)
        {
            mtnDewInv=pepsiInv=aquafinaInv=5;
            machineBank=0;
            cout << endl << "Successfully Restocked!" << endl;
            proceed=false;
            continue;
        }
        
        /*On the condition that selection==5 or when Exit was chosen, the program terminates.
        
          Another method of using the bool 'proceed' would have been to initialize 'proceed' as false at the beginning of the function,
          and only change it to true once, at this else statement. The other method made more intuitive sense to me when I was writing this,
          but this new one would have saved a lot of needless changes of 'proceed'.
        */
        else if (selection==5)
        {
            cout << endl << "Thanks for using Vender by Cinco!" << endl;
        }
    }
    
    //End of do-while loop. If proceed is remains true throughout the entire loop, the program terminates
    while(!proceed);
    return 0;
}

//Displays the vending machine menu and returns the selected value to main. Loops until a valid selection is entered.
int mainMenu()
{
    int selection;
    do
    {
        cout << endl
             << "----Vender by Cinco----\n"
             << "Select an option:\n"
             << "1. Insert Cash\n"
             << "2. Buy Drink\n"
             << "3. Print Status\n"
             << "4. Restock Vender\n"
             << "5. Exit\n";
        cin >> selection;
        if (selection<1||selection>5)
            cout << "Please enter a valid option.\n";
    }
    while (selection<1||selection>5);
    return selection;
}

//Asks how much money the customer enters and returns the value to main. Loops until a valid amount of money is entered.
float insertCash()
{
    float money;
    cout << endl << "How much money would you like to spend?" << endl;
    do
    {
        cin >> money;
        if (money<=0.0)
            cout << endl << "Please enter a positive amount of money." << endl;
    }
    while (money<=0.0);
    return money;
}

//Displays soda choices and returns choice to main. Loops until a valid choice is made.
int sodaSelection()
{
    int choice;
    do
    {
        cout << endl
             << "---Soda---\n"
             << "Select an option:\n"
             << "1. Mountain Dew\n"
             << "2. Pepsi\n"
             << "3. Aquafina\n"
             << "4. Cancel\n"
             << endl
             << "Selection: ";
        cin >> choice;
        if (choice<1||choice>4)
            cout << "Please enter a valid option." << endl;
    }
    while (choice<1||choice>4);
    return choice;
}

//Prints the current status of the vending machine when called
void printStatus(int mtnDewInv, int pepsiInv, int aquafinaInv, float cashInserted, float machineBank)
{
    cout << endl
         << fixed << showpoint << setprecision(2)
         << "---Vender Status---\n"
         << "Mountain Dew Inventory: " << mtnDewInv << " bottles\n"
         << "Pepsi Inventory: " << pepsiInv << " bottles\n"
         << "Aquafina Inventory: " << aquafinaInv << " bottles\n"
         << "Current Customer's Inserted Cash: $" << cashInserted << "\n"
         << "Vending Machine Bank: $" << machineBank << endl;
}
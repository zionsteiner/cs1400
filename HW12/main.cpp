/* Zion Steiner
A02245003
HW12 */

#include <iostream>
#include <iomanip>
#include "PiggyBank.hpp"

using namespace std;

int main()
{
    //0. Sets dollar output format
    cout<<setprecision(2)<<fixed<<showpoint;
    
    //1. Defining instance of PiggyBank class
    PiggyBank myBank;
    
    //2. Tests depositMoney and getSavings functions of PiggyBank class
    cout<<"Depositing $142.42 to PiggyBank MyBank..."<<endl;
    double deposit=142.42;
    myBank.depositMoney(deposit);
    cout<<"Checking balance of MyBank..."<<endl;
    cout<<"$"<<myBank.getSavings()<<endl;
    cout<<endl;
    
    //3.
    cout<<"Depositing $0.01 to MyBank..."<<endl;
    deposit=0.01;
    myBank.depositMoney(deposit);
    cout<<"Checking balance of MyBank..."<<endl;
    cout<<"$"<<myBank.getSavings()<<endl;
    cout<<endl;
    
    //4. Tests negative deposite guard in depositMoney function
    cout<<"Depositing -$10.00..."<<endl;
    deposit=-10.00;
    myBank.depositMoney(deposit);
    cout<<"Checking balance of MyBank..."<<endl;
    cout<<"$"<<myBank.getSavings()<<endl;
    cout<<endl;
    
    //5. Smash MyBank and prints the amount of money that was in the bank
    cout<<"Smashing MyBank..."<<endl;
    double money=myBank.smash();
    cout<<"$"<<money<<" was in MyBank before it was smashed."<<endl;
    cout<<endl;
    
    //6. Attempts deposit 5.00 to a broken bank, then gets and prints current savings
    cout<<"Depositing $5.00..."<<endl;
    deposit=5.00;
    myBank.depositMoney(deposit);
    cout<<"Checking balance of MyBank..."<<endl;
    cout<<"$"<<myBank.getSavings()<<endl;
    cout<<endl;
    
    //7. Creates a parameterized version of PiggyBank with an initial savings of 100.00
    cout<<"PiggyBank myOtherBank is opened with $100.00 to begin."<<endl;
    PiggyBank myOtherBank(100.00);
    cout<<endl;
    
    //8. Gets and prints current savings of myOtherBank
    cout<<"Checking balance of MyOtherBank..."<<endl;
    cout<<"$"<<myOtherBank.getSavings()<<endl;
    cout<<endl;
    
    //9. Deposits 150.00 into myOtherBank and then prints out the savings
    cout<<"Depositing $150.00..."<<endl;
    deposit=150.00;
    myOtherBank.depositMoney(deposit);
    cout<<"Checking balance of MyOtherBank..."<<endl;
    cout<<"$"<<myOtherBank.getSavings()<<endl;
    cout<<endl;
    
    //10. Destructor sequence
    
    return 0;
}
//PiggyBank Implementation File
#include "PiggyBank.hpp"    //Rectangle class definition
#include <iostream>

using namespace std;

//Default Constructor
PiggyBank::PiggyBank()
{
    isBroken=false;
    savings=0.0;
}

//Parameterized Constructor
PiggyBank::PiggyBank(double initialSavings)
{
    isBroken=false;
    savings=initialSavings;
}

//Getter function for savings
double PiggyBank::getSavings() const
{
    return savings;
}

 //Accepts a double to be added to savings
 void PiggyBank::depositMoney(double deposit)
 {
    if(!isBroken && deposit>0.0)
        savings+=deposit;
    if(deposit<=0.0)
        cout<<"You cannot deposit a negative amount!"<<endl;
    if(isBroken)
        cout<<"You cannot deposit money to a broken piggy bank!"<<endl;
 }
 
//Breaks piggy bank, sets isBroken to true, sets savings to 0, and returns savings
double PiggyBank::smash()
{
    double tempSavings=savings;
    isBroken=true;
    savings=0.0;
    return tempSavings;
}

//Destructor
PiggyBank::~PiggyBank()
{
    if(isBroken)
        cout<<"Poor Broken Pig!"<<endl;
}
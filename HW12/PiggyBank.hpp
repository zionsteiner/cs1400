//PiggyBank Specification
#ifndef PIGGYBANK_HPP
#define PIGGYBANK_HPP

class PiggyBank
{
    private:
        bool isBroken;
        double savings;
    public: 
        PiggyBank();         //Default Constructor
        PiggyBank(double);   //Parametered Constructor
        double getSavings() const;    //Getter function for savings
        void depositMoney(double);  //Accepts a double to be added to savings
        double smash();       //Breaks piggy bank, sets isBroken to true, sets savings to 0, and returns savings
        ~PiggyBank();          //Destructor
};

#endif
/* Zion Steiner
   A02245003
   HW11 Rational Numbers */
#include <iostream>

using namespace std;

struct Rational {
	int n, d;
};

void printRational(Rational r);

int gcd(int n, int d);

// Each function returns false if the resulting Rational number would have a
// zero in the denominator.
bool simplify(Rational &r);
bool invert(Rational &r);
bool create(Rational &r, int num = 1, int denom = 1);

// The result of the operation between Rationals a and b is stored in c
bool add(Rational a, Rational b, Rational &c);
bool sub(Rational a, Rational b, Rational &c);
bool mul(Rational a, Rational b, Rational &c);
bool div(Rational a, Rational b, Rational &c);


int main(void) {
    // In a loop, the user is asked which math operation they would like to perform [+, -, *, /]
    //If Q or q is entered, the program exits.
    //The user is continually prompted to enter a valid option until they do.
    
    //Bool run is used to keep loop running.
    //Run is set to false is the user enters q or Q.
    bool run=true;
    while(run){
        
        //Declares rational number data types and char choice.
        Rational r1, r2;
        Rational result;
        char choice;
        
        cout<<"What operation would you like to perform? [+, -, *, /]\n"
            <<"Press Q to quit."<<endl;
        cin>>choice;
        
        //Input validation
        while(choice!='q' && choice!='Q' && choice!='+' && choice!='-' && choice!='*' && choice!='/'){
            cout<<"Please enter a valid option."<<endl;
            cin.ignore(100, '\n');
            cin>>choice;
        }
        
        //Quit
        if(choice=='q' || choice=='Q'){
            cout<<"Goodbye."<<endl;
            run=false;
            continue;
        }
        
        cout<<"Enter the first rational numerator/denominator pair with a space between.\n"
            <<"7/9 as 7 9"<<endl;
            
        //Calls create function and tests for zero in denom.
        //If create returns false, the loop will restart.
        //Create function is used for first rational number data entry.
        if(!create(r1)){
            cout<<"Invalid rational number!"<<endl;
            continue;
        }
        
        //Data entry for second number
        cout<<"Enter the second rational number in the same format."<<endl;
        if(!create(r2)){
            cout<<"Invalid rational number!"<<endl;
            continue;
        }
        //Prints the equation involving the Rational numbers 
        printRational(r1);
        cout<<" "<<choice<<" ";
        printRational(r2);
        cout<<" "<<"= ";
    
        //Perform the arithmetic operation and print the resulting Rational number
        switch(choice){
                    
                      //Calls and tests arithmetic function. If true, the answer to the equation will be printed.
                      //If the function returns false, an error message will appear and the loop will restart.
            case '+': if(add(r1, r2, result))
                        printRational(result);
                      else 
                        cout<<"Error: Invalid rational number";
                      break;
                      
            case '-': if(sub(r1, r2, result))
                        printRational(result);
                      else 
                        cout<<"Error: Invalid rational number";
                      break;
                      
            case '*': if(mul(r1, r2, result))
                        printRational(result);
                      else
                        cout<<"Error: Invalid rational number";
                      break;
            
            case '/': if(div(r1, r2, result))
                        printRational(result);
                      else
                        cout<<"Divide by zero!";
                      break;
        }
        cout<<endl;
    }
    return 0;
}

//Prints the selected rational number.
void printRational(Rational r) {
    if (r.d == 1) {
        cout << r.n;
    }
    else {
        cout << r.n << '/' << r.d;
    }
}

/* Euclid's Algorithm is used here to return when called the greatest common divisor between
a numerator and denominator */
int gcd(int n, int d){
    if(d==0)
        return n;
    else
        return gcd(d, n%d); 
}

//Simplifies the rational number by dividing the num and denom by the GCD.
//If the denom is negative, it will be set to positive and the num will be made negative instead.
bool simplify(Rational &r){
    int gcf=gcd(r.n, r.d);
    r.n/=gcf;
    r.d/=gcf;
    if(r.d<0){
        r.n*=-1;
        r.d*=-1;
    }    
    if(r.d==0)
        return false;
    else
        return true;
}

//Flips the rational number so that the num is the denom and the denom is the num.
bool invert(Rational &r){
    int tempRn=r.n;
    r.n=r.d;
    r.d=tempRn;
    simplify(r);
    if(r.d==0)
        return false;
    else
        return true;
}

//Creates and simplifies a new rational number based on the data passed.
bool create(Rational &r, int num, int denom){
    cin>>num>>denom;
    r.n=num;
    r.d=denom;
    simplify(r);
    if(r.d==0)
        return false;
    else
        return true;
}

//Performs addition on a and b. Result is stored in c.
bool add(Rational a, Rational b, Rational &c){
    c.n = a.n*b.d +  b.n*a.d;
    c.d = a.d*b.d;
    simplify(c);
    //Returns zero is denominator is zero.
    if(c.d==0)
        return false;
    else
        return true;
}

//Performs subtraction on a and b. Result is stored in c.
bool sub(Rational a, Rational b, Rational &c){
    c.n = a.n*b.d - b.n*a.d;
    c.d = a.d*b.d;
    simplify(c);
    if(c.d==0)
        return false;
    else
        return true;
}

//Performs multiplication on a and b. Result is stored in c.
bool mul(Rational a, Rational b, Rational &c){
    c.n = a.n*b.n;
    c.d = a.d*b.d;
    simplify(c);
    if(c.d==0)
        return false;
    else
        return true;
}

//Performs division on a and b. Result is stored in c.
//First, rational number b is inverted. Then multiplication can be used to get rational c.
bool div(Rational a, Rational b, Rational &c){
    invert(b);
    if(mul(a, b, c))
        return true;
    else
        return false;
}

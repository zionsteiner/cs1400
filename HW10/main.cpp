/*Zion Steiner
A02245003
HW10 Mandelbrot Set Image Generator
Note: My HW7 program was referenced for some of this program. Pseudocode and 
files from the homework 10 Canvas page were used as well. The Seahorse Config File
was too large for either C9 or the ppmToBmp to handle, so it had to be downscaled in
order to be generated properly.*/
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "ppmToBmp.hpp" // provides the function int ppmToBmp(std::string ppmFileName);

using namespace std;

struct Color {
    int r;
    int g;
    int b;
};

struct MandelbrotConfig {
    //
    // These values come from the configuration file
    //
    int pixels;            // dimensions of square image, in pixels

    double midX, midY;     // center of image in imaginary coords
    double axisLen;        // width and height of image in imaginary units

    int maxIterations;     // max number of iterations to test for escape

    Color colorOne;        // color of pixels outside of Mandelbrot set
    Color colorTwo;        // color of pixels within Mandelbrot set

    string outputPPMfile;  // name of PPM file to create

    double minX;        // X coords in imaginary units of left & right sides of image
    double minY;    
    double maxX;        // Y coords in imaginary units of top & bottom sides of image
    double maxY;          
    double pixelSize;   // how many imaginary units of length/height each pixel takes

    double step_r;      // how far the red channel changes between iterations
    double step_g;      // how far the green channel changes between iterations
    double step_b;      // how far the blue channel changes between iterations
};



// Read fractal config file and return a MandelbrotConfig struct
MandelbrotConfig readConfig(string configFileLocation);

// Helper function: given a coordinate corresponding to an imaginary number,
// run the Escape Time algorithm and count how many iterations this number takes
int countIterations(MandelbrotConfig config, int col, int row);

// Helper function: decide what color corresponds to a given number of iterations
Color getPixelColor(MandelbrotConfig config, int iterations);

// Create the PPM file, including header, and build the image pixel by pixel
// Loop over the grid of pixels and call the above helper functions as needed
bool drawMandelbrot(MandelbrotConfig config);


int main(int argc, char* argv[]) 
{
    string filename;

    // Read in config file location from user
    cout<<"Enter the location of the desired Mandelbrot config file: ";
    cin>>filename;

    // Create a configuration struct from the file
    MandelbrotConfig cfg = readConfig(filename);

    // Compute and write specified mandelbrot image to PPM file
    if (drawMandelbrot(cfg)) {
        // use the provided function to create a BMP image
        ppmToBmp(cfg.outputPPMfile);
    }
    else {
        return 1;
    }

    return 0;
}

MandelbrotConfig readConfig(string configFileLocation)
{
    MandelbrotConfig cfg;
    
    ifstream fin(configFileLocation);
    
    //Reads in config elements from file to MandelbrotConfig cfg
    fin>>cfg.pixels
       >>cfg.midX>>cfg.midY>>cfg.axisLen
       >>cfg.maxIterations
       >>cfg.colorOne.r>>cfg.colorOne.g>>cfg.colorOne.b
       >>cfg.colorTwo.r>>cfg.colorTwo.g>>cfg.colorTwo.b
       >>cfg.outputPPMfile;
    
    //Calculates and stores remaining cfg variables   
    cfg.minX=cfg.midX - cfg.axisLen/2;        
    cfg.minY=cfg.midY - cfg.axisLen/2;     
    cfg.maxX=cfg.midX + cfg.axisLen/2;
    cfg.maxY=cfg.midY + cfg.axisLen/2;            
    cfg.pixelSize=cfg.axisLen/cfg.pixels;     // how many imaginary units of length/height each pixel takes

    cfg.step_r= (static_cast<double>(cfg.colorOne.r-cfg.colorTwo.r)/(cfg.maxIterations));         
    cfg.step_g= (static_cast<double>(cfg.colorOne.g-cfg.colorTwo.g)/(cfg.maxIterations));         
    cfg.step_b= (static_cast<double>(cfg.colorOne.b-cfg.colorTwo.b)/(cfg.maxIterations));         
    
    return cfg;
}

int countIterations(MandelbrotConfig config, int col, int row)
{
    /*Using the pixels coords and the relative config starting position,
    calculates the pixel's position on the complex plane*/
    double x0=config.minX + col*config.pixelSize;
    double y0=config.maxY - row*config.pixelSize;
    
    double x=0.0;
    double y=0.0;
    
    int iter=0;
    
    /*The loop counts every time that the function iterates without either of the complex number's
    parts having coefficients >2. The loop will also stop once the max iterations have been reached.*/
    while((x*x + y*y)<2*2 && iter<config.maxIterations)
    {
        double xtemp=x*x - y*y + x0;
        y=2*x*y + y0;
        x=xtemp;
        iter++;
    }
    return iter;
}

Color getPixelColor(MandelbrotConfig config, int iterations)
{
    Color pixelColor;
    
    /*Same logic as color smoosh (HW7). The color is calculated based on config.step
    multiplied by the number of iterations that pixel went through.*/
    pixelColor.r= round(config.colorOne.r-config.step_r*iterations);
    pixelColor.g= round(config.colorOne.g-config.step_g*iterations);
    pixelColor.b= round(config.colorOne.b-config.step_b*iterations);
    return pixelColor;
}

bool drawMandelbrot(MandelbrotConfig config)
{
    ofstream fout(config.outputPPMfile);
    
    //ppm file header
    fout<<left<<"P3"<<endl;
    fout<<left<<setw(4)<<config.pixels<<left<<setw(4)<<config.pixels<<endl;
    fout<<left<<255<<endl;
    
    //Outer-loop runs once per row
    for(int i=1; i<=config.pixels; i++)
    {
        //Inner-loop runs once per column, per row 
        for(int j=1; j<=config.pixels; j++)
        {
            //Finds the iterations and associated color of the pixel located at row i, column j
            int iteration= countIterations(config, j, i);
            Color pixelColor= getPixelColor(config, iteration);
            
            //Prints the properly formatted RGB to the .ppm
            fout<<left<<setw(4)<<pixelColor.r
                <<left<<setw(4)<<pixelColor.g
                <<left<<setw(6)<<pixelColor.b;
        }
        
        //Opens new row
        fout<<endl;
    }
    fout.close();
    
    return true;
}
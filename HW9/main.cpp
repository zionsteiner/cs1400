/*Zion Steiner
A02245003
HW9 YAHTZEE REDUX*/



#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>

using namespace std;

//Used throughout program to limit how many rolls to allow the user
const int numberOfRolls=7;

//Function prototypes
bool yahtzee();
bool fourOfAKind();
bool straight();
bool fullHouse();
int reRoll(int, int);
int fullHouseReRoll(int, int, int);

/*The main function displays a menu with a string following each option. The string starts
out as NOT_ATTEMPTED and updates as the game functions return bools for wins and losses.
A switch statement operates as the menu, taking in user selections and calling the appropriate
function. When the user finishes each round, the game will offer them a congratulatory message
if all rounds were completed successfully, and a different message if they lost. Afterwards, the
user is given the option to play the game again. If they choose to play again, the status of each
game is reset to NOT_ATTEMPTED.*/
int main()
{
    //Used to control outer while-loop
    bool play=true;
    
    //Stores the bool returned from each game function
    bool yahtzeeTF, fourOfAKindTF, straightTF, fullHouseTF;
    
    /*Status strings are used to show the status of each round. They update as the
    user completed the rounds.*/
    string yahtzeeStatus="NOT_ATTEMPTED";
    string fourOfAKindStatus="NOT_ATTEMPTED";
    string straightStatus="NOT_ATTEMPTED";
    string fullHouseStatus="NOT_ATTEMPTED";
    
    //Used to check if user wants to replay the game
    char input;
    
    cout<<"YAHTZEE V.2\n"
        <<"Complete all four rounds to win. You have 7 chances to win each round.\n"
        <<"Good luck!!!"<<endl;
        
        while(play)
        {
            //Menu
            cout<<"Please select a round to play:\n"
                <<"1. Yahtzee (Roll 5 dice of the same number)...................."<<yahtzeeStatus<<endl;
                
            cout<<"2. 4 of a kind (Roll 4 dice of the same number)................"<<fourOfAKindStatus<<endl;
               
            cout<<"3. Straight (Roll all 5 dice in sequential order).............."<<straightStatus<<endl;

            cout<<"4. Full House (Roll 3 dice of one number, 2 of another)........"<<fullHouseStatus<<endl;
                
            cout<<"5. Quit Playing"<<endl;
            
            //Win conditions
            if(yahtzeeStatus=="COMPLETE" && fourOfAKindStatus=="COMPLETE" && straightStatus=="COMPLETE" && fullHouseStatus=="COMPLETE")
            {
                cout<<"You won all four rounds! You win!\n"
                    <<"Play again? Enter Y for yes, and anything else to quit."<<endl;
                cin>>input;
                if(input=='y'||input=='Y')
                {
                    yahtzeeStatus=fourOfAKindStatus=straightStatus=fullHouseStatus="NOT_ATTEMPTED";
                    continue;
                }    
                else
                {
                    play=false;
                    cout<<"Thanks for playing!!!"<<endl;
                    break;
                }
            }
            
            //Loss condition
            else if(yahtzeeStatus!="NOT_ATTEMPTED" && fourOfAKindStatus!="NOT_ATTEMPTED" && straightStatus!="NOT_ATTEMPTED" && fullHouseStatus!="NOT_ATTEMPTED")
            {
                cout<<"You didn't win all four rounds, so you lost. Get better.\n"
                    <<"Play again? Enter Y for yes, and anything else to quit."<<endl;
                cin.ignore();
                input=cin.get();
                if(input=='y'||input=='Y')
                {
                    yahtzeeStatus=fourOfAKindStatus=straightStatus=fullHouseStatus="NOT_ATTEMPTED";
                    
                    continue;
                }
                else
                {
                    play=false;
                    cout<<"Thanks for playing!!!"<<endl;
                    break;
                }
            }
            
            int input;
            cin>>input;
            
            //Redirects user to the appropriate game
            switch(input)
            {
                case 1: cout<<"YAHTZEE"<<endl;
                        yahtzeeTF=yahtzee();
                        if(yahtzeeTF==true)
                            yahtzeeStatus="COMPLETE";
                        else if(yahtzeeTF==false)
                            yahtzeeStatus="FAILED";
                        break;
                case 2: cout<<"4 of a Kind"<<endl;
                        fourOfAKindTF=fourOfAKind();
                        if(fourOfAKindTF==true)
                            fourOfAKindStatus="COMPLETE";
                        else if(fourOfAKindTF==false)
                            fourOfAKindStatus="FAILED";
                        break;
                case 3: cout<<"Straight"<<endl;
                        straightTF=straight();
                        if(straightTF==true)
                            straightStatus="COMPLETE";
                        else if(straightTF==false)
                            straightStatus="FAILED";
                        break;
                case 4: cout<<"Full House"<<endl;
                        fullHouseTF=fullHouse();
                        if(fullHouseTF==true)
                            fullHouseStatus="COMPLETE";
                        else if(fullHouseTF==false)
                            fullHouseStatus="FAILED";
                        break;
                case 5: cout<<"Thanks for playing!!!"<<endl;
                        play=false;
                        break;
                
                default: cout<<"Invalid input"<<endl;
                         
            }
        }    
    return 0;
}

//When called, the yahtzee game will play. The function returns true if the player wins, false if they lose.
bool yahtzee()
{
    unsigned int seed=time(0);
    srand(seed);
    
    //Controls while-loop
    bool win=false;
    
    //roll# counter
    int i=1;
    
    int holdnum;
    
    cout<<"You have 7 rolls to get a Yahtzee"<<endl;
    
    //Initializes and displays all die as random numbers 1-6.
    int die1=rand()%6+1, die2=rand()%6+1, die3=rand()%6+1, die4=rand()%6+1, die5=rand()%6+1;
    cout<<"First Roll: "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        
    /*While win is false and rolls is under 7, this loop will ask the user what number they want to hold.
    All die that are not the same as the hold number are assigned another random number 1-6.
    This continues until a yahtzee is reached or the user has used all their rolls. When all 5 die are the same number, a 
    congratulatory message is printed and bool win is set to true.*/
    while(!win && i<=numberOfRolls)
    {
        if(die1==die2 && die2==die3 && die3==die4 && die4==die5)
        {
            win=true;
            cout<<"You gotta YAHTZEE with the number "<<die1<<" in "<<i<<" rolls!!!"<<endl;
            continue;
        }
        
        if(i<numberOfRolls)
        {
            cout<<"Which number would you like to hold?"<<endl;
            cin>>holdnum;
        }    
            
        //Validation for hold number
        while(holdnum!=die1 && holdnum!=die2 && holdnum!=die3 && holdnum!=die4 && holdnum!=die5)
        {
            cout<<"You can only hold a number if you already have a die of that number.\nPlease enter a valid number."<<endl;
            cin>>holdnum;
        }
        
        //Roll incrementation
        i++;
        
        if(i<=numberOfRolls)
        {
        die1=reRoll(die1, holdnum), die2=reRoll(die2, holdnum), die3=reRoll(die3, holdnum), die4=reRoll(die4, holdnum), die5=reRoll(die5, holdnum);
        cout<<"Roll #"<<i<<": "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        }
    }
    
    //Returns true if the user won, returns false if they failed
    if(win==true)
        return true;
    else
    {
        cout<<"You lost this round!"<<endl;
        return false;
    }
}

//Returns true when user wins, returns false when user loses. 
bool fourOfAKind()
{
    bool win=false;
    
    unsigned int seed=time(0);
    srand(seed);
    
    //roll# counter
    int i=1;
    
    int holdnum;
    
    cout<<"You have 7 rolls to get a four of a kind"<<endl;
     
    //Initializes and displays all die as random numbers 1-6.
    int die1=rand()%6+1, die2=rand()%6+1, die3=rand()%6+1, die4=rand()%6+1, die5=rand()%6+1;
    cout<<"First Roll: "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        
    /*While win is false and rolls is under 7, this loop will ask the user what number they want to hold.
    All die that are not the same as the hold number are assigned another random number 1-6.
    This continues until a four of a kind is reached or the user has used all their rolls. When 4 die are the same number, a 
    congratulatory message is printed and bool win is set to true.*/
    while(!win && i<=numberOfRolls)
    {
        /*Counter counts how many instances of each number 1-6 occur. If a number 
        occurs 4 or more times, win is set to true and the while loop breaks.*/
        for(int j=1; j<=6; j++)
        {
            int count=0;
            if(die1==j)
                count++;
            if(die2==j)
                count++;
            if(die3==j)
                count++;
            if(die4==j)
                count++;
            if(die5==j)
                count++;
            if(count>=4)
            {
                win=true;
                cout<<"You gotta four of a kind with the number "<<j<<" in "<<i<<" rolls!!!"<<endl;
            }
        }
        if(win==true)
            continue;
        
        //Lets user select which dice they want to keep
        if(i<numberOfRolls)
        {
            cout<<"Which number would you like to hold?"<<endl;
            cin>>holdnum;
        }
            
        //Validation for hold number
        while(holdnum!=die1 && holdnum!=die2 && holdnum!=die3 && holdnum!=die4 && holdnum!=die5)
        {
            cout<<"You can only hold a number if you already have a die of that number.\nPlease enter a valid number."<<endl;
            cin>>holdnum;
        }
        
        //Roll incrementation
        i++;
        
       if(i<=numberOfRolls)
       {
            die1=reRoll(die1, holdnum), die2=reRoll(die2, holdnum), die3=reRoll(die3, holdnum), die4=reRoll(die4, holdnum), die5=reRoll(die5, holdnum);
            cout<<"Roll #"<<i<<": "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
       }
    }
    
    //Returns true if the user won, returns false if they failed
    if(win==true)
        return true;
    else
    {
        cout<<"You lost this round!"<<endl;
        return false;
    }
}

/*Plays the straight game. The user tells the program what to reroll by entering
1 to hold or 0 to roll. */
bool straight()
{
    bool win=false;
    
    unsigned int seed=time(0);
    srand(seed);
    
    //roll counter
    int i=1;
    
    //1's and 0's are stored in these chars to control which dice to keep and which to roll.
    char roll1, roll2, roll3, roll4, roll5;
    
    cout<<"You have 7 rolls to get a straight"<<endl;
    
    //Initializes and displays all die as random numbers 1-6.
    int die1=rand()%6+1, die2=rand()%6+1, die3=rand()%6+1, die4=rand()%6+1, die5=rand()%6+1;
    cout<<"First Roll: "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
    
    /*Using chars roll[x] the loop rerolls selected dice. If the win condition is met,
    a true is returned to main.*/
    while(!win && i<=numberOfRolls)
    {
        //First win condition. If the outer condition is met, the inner condition is checked. If both are met, the user wins the round.
        if((die1+die2+die3+die4+die5==15)||(die1+die2+die3+die4+die5==20))
        {
            if((die1!=die2 && die1!=die3 && die1!=die4 && die1!=die5) && (die2!=die3 && die2!=die4 && die2!=die5) && (die3!=die4 && die3!=die5) && (die4!=die5))
            {
                win=true;
                cout<<"You got a straight! Good job!"<<endl;
                continue;
            }
        }
        
        //Roll incrementation
        i++;
        
        if(i<=numberOfRolls)
        {
            cout<<"Enter consecutive 1's and 0's to specify which dice to hold.\n"
                <<"For example, enter 11000 if you want to hold the first 2 dice and roll the last 3.\n"
                <<"To hold the first, middle, and last, enter 10101."<<endl;
            cin.ignore();
            cin.get(roll1);
            cin.get(roll2);
            cin.get(roll3);
            cin.get(roll4);
            cin.get(roll5);
            if(roll1=='0')
                die1=rand()%6+1;
            if(roll2=='0')
                die2=rand()%6+1;
            if(roll3=='0')
                die3=rand()%6+1;
            if(roll4=='0')
                die4=rand()%6+1;
            if(roll5=='0')
                die5=rand()%6+1;
            cout<<"Roll #"<<i<<": "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        }
    }
    
    //Returns true if the user won, returns false if they failed
    if(win==true)
        return true;
    else
    {
        cout<<"You lost this round!"<<endl;
        return false;
    }
}

/*Runs similar to the fourOfAKind function. Instead of only choosing one number to hold,
the user is able to choose two numbers. If the user gets both a three of a kind and a two of a kind,
the win condition is met and a true is returned to main.

are*/
bool fullHouse()
{
    //Variables
    bool win=false;
    bool threeOfAKindTF=false;
    bool twoOfAKindTF=false;
    unsigned int seed=time(0);
    
    //Seeds rand
    srand(seed);
    
    //roll# counter
    int i=1;
    
    int holdnum1,holdnum2;
    
    /*Used to return bools threeOfAKindTF and twoOfAKindTF to false if the number 
    that originally satisfied the condition no longer does.*/
    int threeOfAKindNum, twoOfAKindNum;
    threeOfAKindNum=twoOfAKindNum=0;
    
    cout<<"You have 7 rolls to get a four of a kind"<<endl;
     
    //Initializes and displays all die as random numbers 1-6.
    int die1=rand()%6+1, die2=rand()%6+1, die3=rand()%6+1, die4=rand()%6+1, die5=rand()%6+1;
    cout<<"First Roll: "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        
    while(!win && i<=numberOfRolls)
    {
        /*Counts how many times each number 1-6 occurs in the dice roll. When three
        dice are the same number, threeOfAKindTF is set to true.*/
        for(int j=1; j<=6; j++)
        {
            int count=0;
            if(die1==j)
                count++;
            if(die2==j)
                count++;
            if(die3==j)
                count++;
            if(die4==j)
                count++;
            if(die5==j)
                count++;
            if(count==3)
            {
                threeOfAKindTF=true;
                threeOfAKindNum=j;
            }
            else if(count!=3 && threeOfAKindNum==j)
                threeOfAKindTF=false;
        }
        for(int k=1; k<=6; k++)
        {
            int count=0;
            if(die1==k)
                count++;
            if(die2==k)
                count++;
            if(die3==k)
                count++;
            if(die4==k)
                count++;
            if(die5==k)
                count++;
            if(count==2)
            {
                twoOfAKindTF=true;
                twoOfAKindNum=k;
            }
            else if(count!=2 && twoOfAKindNum==k)
                threeOfAKindTF=false;
        }
        //Win condition
        if(threeOfAKindTF==true && twoOfAKindTF==true)
        {
            cout<<"You won this round. GOOD job."<<endl;
            win=true;
            continue;
        }
        
        //Holdnum variable holds which number dice the user wants to hold
        if(i<numberOfRolls)
        {
            cout<<"Which two numbers would you like to hold? Press enter after entering each number."<<endl;
            cin>>holdnum1;
            cin>>holdnum2;
        }    
        
        //Increments roll number
        i++;
            
        //Validation for hold number
        while(holdnum1!=die1 && holdnum1!=die2 && holdnum1!=die3 && holdnum1!=die4 && holdnum1!=die5)
        {
            cout<<"You can only hold a number if you already have a die of that number.\nPlease enter a valid number."<<endl;
            cin>>holdnum1;
        }
        while(holdnum2!=die1 && holdnum2!=die2 && holdnum2!=die3 && holdnum2!=die4 && holdnum2!=die5)
        {
            cout<<"You can only hold a number if you already have a die of that number.\nPlease enter a valid number."<<endl;
            cin>>holdnum2;
        }
        
        //Rerolls selected dice
        if(i<=numberOfRolls)
        {
            die1=fullHouseReRoll(die1, holdnum1, holdnum2), die2=fullHouseReRoll(die2, holdnum1, holdnum2), die3=fullHouseReRoll(die3, holdnum1, holdnum2), die4=fullHouseReRoll(die4, holdnum1, holdnum2), die5=fullHouseReRoll(die5, holdnum1, holdnum2);
            cout<<"Roll #"<<i<<": "<<die1<<" "<<die2<<" "<<die3<<" "<<die4<<" "<<die5<<endl;
        }
    }
    
    //Returns true if the user won, returns false if they failed
    if(win==true)
        return true;
    else
    {
        cout<<"You lost this round!"<<endl;
        return false;
    }   
}

/*Used for yahtzee and four of a kind functions. When called, this function checks 
the die number. If the number is the same as the hold number, it passes back the
original die number. If not, a new random number 1-6 is generated and passed back
to that die.*/
int reRoll(int die, int holdnum)
{
    if(die!=holdnum)
        return rand()%6+1;
    else
        return die;
}

int fullHouseReRoll(int die, int holdnum1, int holdnum2)
{
    if(die!=holdnum1 && die!=holdnum2)
        return rand()%6+1;
    else
        return die;
}
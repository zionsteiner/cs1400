/*Smoosh Color Generator
  Zion Steiner
  A02245003
  October 14th, 2017*/
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>

using namespace std;

//data construct to contain red, green and blue integers for left and right
struct Color
{
    int r, g, b;
};

//smoosh generator prototype
void generateGradient(int, int, int, int, int, int, int, int, string);

int main()
{
    Color leftColor, rightColor;
    int height, width;
    
    string filename;
    
    //reads in left RGB
    cout<<"This is a color-gradience generator program.\n"
        <<"Please enter the left color: \n"
        <<"(Please enter the color in RGB form)"<<endl;
    cin>>leftColor.r>>leftColor.g>>leftColor.b;
    
    //If a color value is not within the correct range, these statements will set that color to the nearest acceptable value
    if(leftColor.r<0)
        leftColor.r=0;
    else if(leftColor.r>255)
        leftColor.r=255;
        
    if(leftColor.g<0)
        leftColor.g=0;
    else if(leftColor.g>255)
        leftColor.g=255;
        
    if(leftColor.b<0)
        leftColor.b=0;
    else if(leftColor.b>255)
        leftColor.b=255;
        
    //reads in right RGB    
    cout<<"Enter the RGB for the right color."<<endl;
    cin>>rightColor.r>>rightColor.g>>rightColor.b;
    
    if(rightColor.r<0)
        rightColor.r=0;
    else if(rightColor.r>255)
        rightColor.r=255;
        
    if(rightColor.g<0)
        rightColor.g=0;
    else if(rightColor.g>255)
        rightColor.g=255;
        
    if(rightColor.b<0)
        rightColor.b=0;
    else if(rightColor.b>255)
        rightColor.b=255;
    
    //reads in picture dimension info
    cout<<"Enter the desired height of the image: ";
    cin>>height;
    cout<<endl<<"Enter the width of the image: ";
    cin>>width;
    
    //reads the desired filename into a string
    cout<<"What would you like to name this file?"<<endl;
    cin>>filename;
    
    //Calls smoosh generator
    generateGradient(leftColor.r, leftColor.g, leftColor.b, rightColor.r, rightColor.g, rightColor.b, height, width, filename);
    
    return 0;
}

//This function sends the correct information to the ppm file
void generateGradient(int leftRed, int leftGreen, int leftBlue, int rightRed, int rightGreen, int rightBlue, int height, int width, string filename)
{
    ofstream fout(filename);
    
    //ppm file header
    fout<<left<<"P3"<<endl;
    fout<<left<<setw(4)<<width<<left<<setw(4)<<height<<endl;
    fout<<left<<255<<endl;
    
    //Outer for-loop runs the inner for-loop once for each unit of height
    for(int i=1; i<=height; i++)
    {
        /*Inner for-loop calculates the RGB of each pixel and sends it to ppm file. RGB of each pixel is calculated by taking the difference of the left and right colors,
          and dividing that by the width-1. This value represents how much a color needs to be changed per column. The value is then rounded and multiplied by i, or the column number. 
          The initial left colors will be decremented or incremented by that value (depending on if right color is greater than left color value) and will eventually reach the 
          value of the right colors on the last column.*/
        for(int i=0; i<width; i++)
        {
            fout<<left<<setw(4)<<leftRed-round((static_cast<float>(leftRed-rightRed)/(width-1))*i)
                <<left<<setw(4)<<leftGreen-round((static_cast<float>(leftGreen-rightGreen)/(width-1))*i)
                <<left<<setw(6)<<leftBlue-round((static_cast<float>(leftBlue-rightBlue)/(width-1))*i);
        }
        
        //Opens new row
        fout<<endl;
    }
    fout.close();
}
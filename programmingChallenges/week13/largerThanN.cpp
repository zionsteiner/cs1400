#include <iostream>
using namespace std;

void largerThanN(int*, int, int);

int main()
{
    int sizeOf=25;
    int cards[sizeOf];
    int N;
    
    for(int i=0; i<sizeOf; i++)
    {
        cards[i]=i+1;
    }
    
    cout<<"What number?"<<endl;
    cin>>N;
    cout<<endl;
    
    
    largerThanN(cards, sizeOf, N);
    
    return 0;
}

void largerThanN(int* cards, int sizeOf, int N)
{
    for(int i=0; i<sizeOf; i++)
    {
        if(cards[i]>N)
            cout<<cards[i]<<endl;
    }
}
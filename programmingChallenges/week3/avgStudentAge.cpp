//Average Student Age Calc
/* Zion Steiner
A02245003
CS1400 Recitation Sec 508
*/
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
    ifstream fin;
    fin.open("data/studentAges.txt");
    
    string name;
    int age=0;
    int count=0;
    int num=0;
    while(fin.eof()==false)
    {
        fin>>name;
        fin>>age;
        count=count+age;
        num++;
    }
    float average= (float) count/num;
    cout<<fixed<<showpoint<<setprecision(1)<<"The average age of students for this class is: "<<average<<" years old."<<endl;
    
    
    
    
    return 0;
}
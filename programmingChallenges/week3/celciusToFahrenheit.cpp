#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
       cout << "Celcius\t\tFahrenheit\n"
             << "----------------------------\n";
    for (int count=0; count<=20; count++)
    {
        float degreeF = static_cast<float> (9)/(5)*count + 32;
        cout << fixed << setprecision (1) << showpoint
             << count << "\t\t" << degreeF << endl;
    }
    return 0;
}
//Distance Traveled Calc
#include <iostream>
using namespace std;

int main()
{
    int hours,      //Define variables
        speedMPH;
    cout << "This program will calculate distance traveled, given the time\n"       
         << " traveled and speed.\n"
         << "Please enter how long the vehicle was traveling (in hours).\n";        //Ask user to enter hours traveled
    cin >> hours;
    while (hours<1)                                                                 //While-loop for hours input validation
    {
        cout << "Please enter a whole number over one.\n";                          
        cin >> hours;
    }
    cout << "What was the average velocity of the vehicle?\n";                      //Ask user to enter speed
    cin >> speedMPH;
    while (speedMPH<0)                                                              //While-loop for speed input validation
    {
        cout << "ERROR: Enter a value for speed greater than 0.\n";
        cin >> speedMPH;
    }   
    cout << "Hour\t\tDistance Traveled (Miles)\n"                                   //Table header
         << "------------------------------------------\n";
    for (int count=1; count<=hours; count++)                                        //for-loop to calculate and display distanced traveled, incremented by the hour
    {
        int distanceTraveled = count*speedMPH;
        cout << count << "\t\t" << distanceTraveled << endl;
    }
         
    return 0;     
}
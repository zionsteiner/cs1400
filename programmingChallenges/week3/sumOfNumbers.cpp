#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int posInt,
        sum=0;
    cout << "Enter a positive whole number.\n";
    cout << "This number and all positive integers less than it will be summed.\n";
    cin >> posInt;
    while (posInt<=0)
    {
        cout << "Remember to enter only positive integers." << endl;
        cin >> posInt;
    }
    for (int count=1; count<=posInt; count++)
    {
        sum+=count;
    }
    cout << "Sum: " << sum << endl;
    return 0;
}
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int floors=0,
        roomsA=0,
        occupiedRoomsA=0;
    cout << "How many floors does the hotel have?\n";
    cin >> floors;
    for (int count=1; count<=floors; count++)
    {
        int rooms,
            occupiedRooms;
        if (count==13)
            continue;
        cout << "Please enter how many rooms are on floor " << count << ".\n";
        cin >> rooms;
        cout << "How many of these rooms are occupied?\n";
        cin >> occupiedRooms;
        
        roomsA+=rooms;
        occupiedRoomsA+=occupiedRooms;
        
    }
    cout << "Total number of rooms: " << roomsA << "\n"
         << "Number of occupied rooms: " << occupiedRoomsA << "\n"
         << fixed << showpoint << setprecision(2)
         << (static_cast<float> (occupiedRoomsA)/roomsA)*100 << "% of rooms are occupied."
         << endl;
    return 0;
}
/* Zion Steiner
A02245003
Work session 7 */


#include <iostream>

using namespace std;

struct rational{
    int numerator, denominator;
};

struct color{
  int r, g, b;  
};

struct zebra{
    int whiteStripes, blackStripes;
};

void print(rational r){
    cout<<"The numerator is: "<<r.numerator<<"."<<endl;
    cout<<"The denominator is: "<<r.denominator<<"."<<endl;
}
void print(color c){
    cout<<"Red: "<<c.r<<endl;
    cout<<"Green: "<<c.g<<endl;
    cout<<"Blue: "<<c.b<<endl;
}

void print(zebra z){
    cout<<"Number of white stripes: "<<z.whiteStripes<<"."<<endl;
    cout<<"Number of black stripes: "<<z.blackStripes<<"."<<endl;
}

rational add(rational r1, rational r2){
    rational rSum;
    rSum.numerator=r1.numerator*r2.denominator + r2.numerator*r1.denominator;
    rSum.denominator= r1.denominator*r2.denominator;
    return rSum;
}

color add(color c1, color c2){
    color Final;
    Final.r = (c1.r+c2.r)/2;
    Final.g = (c1.g+c2.g)/2;
    Final.b = (c1.b+c2.b)/2;
    return Final;
}

zebra add(zebra z1, zebra z2){
    zebra zSum;
    zSum.whiteStripes = z1.whiteStripes + z2.whiteStripes;
    zSum.blackStripes = z1.blackStripes + z2.blackStripes;
    return zSum;
}

int main(){
    rational rat1;
    rat1.numerator=1;
    rat1.denominator=2;
    
    rational rat2;
    rat2.numerator = 9;
    rat2.denominator = 13;
    
    color col1;
    col1.r = 100;
    col1.g = 150;
    col1.b = 160;
    
    color col2;
    col2.r = 134;
    col2.g = 211;
    col2.b = 056;
    
    zebra Alex;
    Alex.whiteStripes = 28;
    Alex.blackStripes = 0;
    
    zebra Zumbaru;
    Zumbaru.whiteStripes = 0;
    Zumbaru.blackStripes = 23;
    
    cout<<"Let's look at our first rational number:"<<endl;
    print(rat1);
    cout<<endl;
    cout<<"Here's the second rational number:"<<endl;
    print(rat2);
    cout<<endl;
    cout<<"The sum of the two rational numbers is: ";
    rational ratSum= add(rat1, rat2);
    cout<<ratSum.numerator<<"/"<<ratSum.denominator<<endl;
    cout<<endl;
    
    cout<<"Now, we shall look at our first color:"<<endl;
    print(col1);
    cout<<endl;
    cout<<"Now the second color:"<<endl;
    print(col2);
    cout<<endl;
    cout<<"The sum of the two colors is: "<<endl;
    print(add(col1, col2));
    cout<<endl;
    
    cout<<"Here is the information for zebra Alex:"<<endl;
    print(Alex);
    cout<<endl;
    cout<<"Information for zebra Zumbaru:"<<endl;
    print(Zumbaru);
    cout<<endl;
    cout<<"The sum of stripes between the two zebras is displayed below:"<<endl;
    print(add(Alex, Zumbaru));
    
    return 0;
}
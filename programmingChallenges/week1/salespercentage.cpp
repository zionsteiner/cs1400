//predicts sales of east coast div
#include <iostream>
using namespace std;

int main()
{
    //defines variables
   const float percentage = 0.58,
           totalsalesinmillions = 8.6;
    //calculates east coast sales
    double eastcoastsales = percentage * totalsalesinmillions;
    //displays eastcoastsales
    cout <<"The East Coast division of Company X will make about $ " << eastcoastsales << " million in sales." <<endl;
    return 0;
}
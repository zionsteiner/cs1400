//computes sales tax
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    //define variables
 const   float purchase = 95,
          stateTax = 0.065,
          countyTax = 0.02,
          totalCost = purchase*(1+stateTax+countyTax);
     //set info to display     
    cout << "Purchase Price: $" << purchase <<endl
    << "State Sales Tax: " << stateTax*100 << "%" <<endl
    << "County Sales Tax: " << countyTax*100 << "%" <<endl
    << "Your total cost is: $" << setprecision(2) << fixed << totalCost <<endl;
    return 0;
}
               
               
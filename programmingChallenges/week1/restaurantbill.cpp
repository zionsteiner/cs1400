//computes cost of meal with tip and tax
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{   //variable definitions
    const float mealCharge = 44.50,
          tax = .0675,
          tip = mealCharge*(1+tax)*(0.15),
          totalcost = tip+mealCharge*(1+tax);
    //set info to display
    cout << "Cost of Meal: $" << setprecision(2) << fixed << mealCharge <<endl
    << "Tax: " << tax*100 << "%" <<endl
    << "Recommended Tip: $" << setprecision(2) << fixed << tip <<endl
    << "Bill Total: $" << setprecision(2) << fixed<< totalcost <<endl;
    return 0;
}
//Program that calculates the sum of two numbers
#include <iostream>
using namespace std;

int main()
{
  int  number1 = 50,
       number2 = 100,
       total;
    
    total = number1 + number2;
    cout << "The total of 50+100 is " << total <<"." <<endl;
    
    return 0;
}
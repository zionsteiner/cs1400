//Practice Program
//this tells the preprocessor to include the iostream file in the program
#include <iostream>
//this tells the program what namespace it will access
using namespace std;
//this is the main function
int main()
{
   int days, years;
   cout <<"How many years old are you? ";
   cin >>years;
   days = years*365;
   cout <<"You are over " << days;
   cout << " days old!";
   
    return 0;
}
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

int main()
{
    const float countyTax=0.02, stateTax=0.04;
    string month;
    float totalCollected;
    cout << "What month would you like to create a report for?" << endl;
    cin >> month;
    cout << "What was the total amount collected during the month (including sales tax)?" << endl;
    cin >> totalCollected;
    cout << "Month: " << month << endl;
    cout << "-------------------" << endl;
    cout << fixed << setprecision(2) << "Total Collected: $" << totalCollected << endl;
    float sales=totalCollected/1.06;
    cout << fixed << setprecision(2) << "Sales: $" << sales << endl;
    cout << fixed << setprecision(2) << "County Sales Tax: $" << sales*countyTax << endl;
    cout << fixed << setprecision(2) << "State Sales Tax: $" << sales*stateTax << endl;
    cout << fixed << setprecision(2) << "Total Sales Tax: $" << sales*(countyTax+stateTax) << endl;
    return 0;
}
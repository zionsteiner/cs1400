#include <iostream>
using namespace std;

int main()
{
float palletWgt,
      loadedPalletWgt,
      widgetWgt=12.5; 
cout << "What is the weight of the empty pallet in pounds?" << endl;
cin >> palletWgt;
cout << "What is the weight of the pallet loaded with widgets?" << endl;
cin >> loadedPalletWgt;
cout << "There are " << (loadedPalletWgt-palletWgt)/widgetWgt << " widgets on the pallet." << endl;
 return 0;   
}
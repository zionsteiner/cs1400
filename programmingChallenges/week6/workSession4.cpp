#include <iostream>
#include <fstream>
using namespace std;

void printMoney(float balance);
float performTransaction(float balance, float value);

int main()
{
    bool done=false;
    float balance=0;
    ifstream fin("money.txt");
    cout << "Welcome to ATM" << endl; 
    while (done==false && !fin.eof())
    {
        cout << "To perform the next transation, press N. Otherwise, press E to exit." << endl;
        char input;
        cin >> input;
        if(input=='n'||input=='N')
        {
            float value;
            fin >> value;
            balance=performTransaction(balance, value);
            printMoney(balance);
        }
        else if (input=='e'||input=='E')
            done=true;
        else
            cout << "Please enter a valid key." << endl;
    }
    return 0;
}

void printMoney(float balance)
{
    cout << "Current Account Balance: $" << balance << endl;
}

float performTransaction(float balance, float value)
{
    return balance+value;
}
//recommended property insurance
#include <iostream>
using namespace std;

int main()
{
    float propertyVal;
    
    cout << "What is the replacement value of your property?" << endl;
    cin >> propertyVal;
    
    float recInsurance = propertyVal*0.80;
    
    cout << "Based on the value of your property, it would be wise to purchase \n"
         << "insurance of at least $" << recInsurance << "." << endl;
         
    return 0;
}
//Stadium Income Calc
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int ClassA$ = 15,
        ClassAQuant,
        ClassB$ = 12,
        ClassBQuant,
        ClassC$ = 9,
        ClassCQuant;
        
    cout << "How many Class A tickets were sold?" <<endl;
    cin >> ClassAQuant;
    cout << "How many Class B tickets were sold?" <<endl;
    cin >> ClassBQuant;
    cout << "How many Class C tickets were sold?" <<endl;
    cin >> ClassCQuant;
    
    float TotalIncome = (ClassA$*ClassAQuant)+(ClassB$*ClassBQuant)+(ClassC$*ClassCQuant);
    
    cout << "Total income from ticket sales is $ " << setprecision(2) << fixed << TotalIncome << endl;
    
    return 0;
}
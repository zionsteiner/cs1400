//determines which of two numbers is larger
#include <iostream>
using namespace std;

int main ()
{
    float num1,
          num2;
          
    cout << "This program will tell you which of two numbers is the largest, \n"
         << "and which is smallest. \n"
         << "Enter the first number." << endl;
    cin >> num1;
    cout << "Enter the second number." << endl;
    cin >> num2;
    
    if (num1>num2)
    cout << num1 << " is larger than " << num2 << "." << endl;
    
    if (num2>num1)
    cout << num2 << " is larger than " << num1 << "." << endl;
    
    return 0;
}
//Magic Date Program
#include <iostream>
using namespace std;

int main()
{
    int month,
        day,
        year;
    
    cout << "This program will determine if the entered date is magic. \n"
         << "Enter the date's month in numberic form (i.e. 9 for September)" << endl;
    cin >> month;
    cout << "Next, enter the day." << endl;
    cin >> day;
    cout << "Finally, enter the last two digits of the year." << endl;
    cin >> year;
    
    if (month*day==year)
    cout << "Wow! That date is magic!" << endl;
    
    if (month*day!=year)
    cout << "Looks like that date is nothing special. Restart to enter another date!" << endl;
    
    return 0;
}
//housing cost calc - for example, you did a workout program
/* Zion Steiner
    A02245003
    CS1400 Recitation 508
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float propertyPayment,                                                      
          utilitiesPayment,
          phonePayment,
          cablePayment;
          
          
    cout << "Hello! This program will find your monthly and annual housing costs. \n"   
         << "Please enter your monthly rent/mortgage payment." << endl;                 
    cin >> propertyPayment;                                                     
    cout << "Please enter what you pay monthly for utilities. \n";
    cin >> utilitiesPayment;
    cout << "Please enter your monthly phone bill. \n";
    cin >> phonePayment;
    cout << "How much do you pay for cable per month? \n";
    cin >> cablePayment;
    
    float monthlyPayment = propertyPayment+utilitiesPayment+phonePayment+cablePayment;  
    
    cout << "Your total monthly housing cost is: $" << setprecision(2) << fixed << monthlyPayment << endl; 
    
    float yearlyPayment = monthlyPayment*12;     
    
    cout << "You pay $" << setprecision(2) << fixed << yearlyPayment << " per year." << endl; 
    
    return 0;
}
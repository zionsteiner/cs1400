//MPG calc
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int fuelCapacity, milesOnFuel;
    cout << "How many gallons of fuel can your vehicle hold? \n";
    cin >> fuelCapacity ;
    cout << "How many miles can your vehicle travel on a full tank? \n";
    cin >> milesOnFuel ;
    
    float mpg = static_cast<float>(milesOnFuel)/fuelCapacity;
    cout << "Your vehicle gets " << setprecision(3) << mpg <<" miles per gallon. \n";
    
    return 0;
}
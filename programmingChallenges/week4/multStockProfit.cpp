#include <iostream>
#include <iomanip>
using namespace std;

float stockProfit(int, float, float, float, float);
int main()
{
    int NS, numberOfTrades;
    float PP, SP, PC, SC, total;
    cout << "This program will calculate the profit/loss of a specified stock trade(s).\n"
         << "The following information will be needed for the calculations.\n"
         << "How many trades would you like to enter?" << endl;
    cin >> numberOfTrades;
    for (int count=1; count<=numberOfTrades; count++) 
    {
        cout << "How many shares of stock " << count << " were sold?" << endl;
        cin >> NS;
        cout << "How much money was each of stock " << count << " sold for?" << endl;
        cin >> SP;
        cout << "How much was each of stock " << count << " initially purchased for?" << endl;
        cin >> PP;
        cout << "What was the purchase commision fee of trade #" << count << "?" << endl;
        cin >> PC;
        cout << "What was the selling commision fee? of trade #" << count << "?" << endl;
        cin >> SC;
        total+= stockProfit(NS, SP, PP, PC, SC);
    }
    cout << "The total ";
     if (total>=0.00)
    {
        cout << "profit";
    }
    else
    {
        cout << "loss";
    }
    cout << " of the " << numberOfTrades << " trade(s) is $" << fixed << showpoint << setprecision(2) << total
         << "." << endl;
}
float stockProfit(int NS, float SP, float PP, float PC, float SC)
{
    return ((static_cast<float>(NS)*SP)-SC)-((static_cast<float>(NS)*PP)-PC);
}
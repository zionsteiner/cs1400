#include <iostream>
#include <iomanip>
using namespace std;

double getSales(string);
void findHighest(double, double, double, double);
int main()
{
    double NE, SE, NW, SW;
    cout << "This program determines which division had the highest sales this quarter.\n";
    NE = getSales("Northeast");
    SE = getSales("Southeast");
    NW = getSales("Northwest");
    SW = getSales("Southwest");
    
    findHighest(NE, SE, NW, SW);
    
    return 0;
}
double getSales(string division)
{
    double sales;
    cout << "What is the " << division << " division's quarterly sales amount?\n";
    cin >> sales;
    
    while (sales<0.00)
        cout << "Error. Please enter a positive sales figure.\n";
        
    return sales;
    
}
void findHighest(double NE, double SE, double NW, double SW)
{
    double highest;
    cout << "The highest grossing division is the ";
    if (NE > SE && NE > NW && NE > SW)
    {
        highest = NE;
        cout << "Northeast";
    }
    else if (SE > NE && SE > NW && SE > SW)
    {
        highest = SE;
        cout << "Southeast";
    }
    else if (NW > NE && NW > SE && NW > SW)
    {
        highest = NW;
        cout << "Northwest";
    }
    else
    {
        highest = SW;
        cout << "Southwest";
    }
    cout << fixed << showpoint << setprecision(2) << " with $" << highest << " in sales." << endl;
}
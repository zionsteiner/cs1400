#include <iostream>
#include <iomanip>
using namespace std;

float stockProfit(int, float, float, float, float);
int main()
{
    int NS;
    float PP, SP, PC, SC;
    cout << "This program will calculate the profit/loss of a specified stock trade.\n"
         << "The following information will be needed for the calculations." << endl;
    cout << "How many shares of the stock were sold?" << endl;
    cin >> NS;
    cout << "How much money was each stock sold for?" << endl;
    cin >> SP;
    cout << "How much was each stock initially purchased for?" << endl;
    cin >> PP;
    cout << "What was the purchase commision fee?" << endl;
    cin >> PC;
    cout << "What was the selling commision fee?" << endl;
    cin >> SC;
    cout << "Computing...\nPlease Wait." << endl;
    cout << "The ";
    float profit = stockProfit(NS, SP, PP, PC, SC);
    if (profit>=0.00)
    {
        cout << "profit";
    }
    else
    {
        cout << "loss";
    }
    cout << " of the trade is $" << fixed << showpoint << setprecision(2) << profit
         << "." << endl;
    return 0;
}
float stockProfit(int NS, float SP, float PP, float PC, float SC)
{
    return ((static_cast<float>(NS)*SP)-SC)-((static_cast<float>(NS)*PP)-PC);
}
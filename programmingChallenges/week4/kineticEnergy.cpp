//kineticEngery calc
#include <iostream>
#include <iomanip>
using namespace std;

float kineticEnergy(float,float);
int main()
{
    float mass, velocity;
    cout << "This calculator will find an object's kinetic energy.\n"
         << "What is the mass of the object? (kg)" << endl;
    cin >> mass;
    cout << "What is the object's velocity?(meters/second)" << endl;
    cin >> velocity;
    
    cout << "Thank you. The object's kinetic energy is " << fixed << setprecision(1) << showpoint
         << kineticEnergy(mass, velocity) << " Joules.";
    return 0;
}

float kineticEnergy(float a, float b)
{
    return (static_cast<float>(1)/2)*a*b*b;
}
//Markup Calculator
#include <iostream>
#include <iomanip>
using namespace std;

float calculateRetail(float, float);

int main()
{
    float wholesalePrice, markupPercentage, retailPrice;
    
    cout << "What is the wholesale price of the item?\n";
    cin >> wholesalePrice;
    cout << "What is the item's desired markup percentage? (Enter as a decimal value less than 1)\n";
    cin >> markupPercentage;
    retailPrice = calculateRetail(wholesalePrice, markupPercentage);
    cout << "The retail price of the item is $" << showpoint << fixed << setprecision(2) << retailPrice << "." << endl;
    return 0;
}

float calculateRetail(float a, float b)
{
    return a+a*b;
}
/*Fahrenheit-Celcius table using for loop and celcius function
Zion Steiner
A02245003
Example was kg to pound conversion using toPounds function*/

#include <iostream>
#include <iomanip>
using namespace std;

//celcius function prototype
float celcius(int);

int main()
{
    cout << "Fahrenheit\t\tCelcius\n"
         << "********************************" << endl;
    /*for-loop that counts 0-20, feeding each iteration of fahrenheit to function "celcius",
    which returns the celcius equivalent of fahrenheit. */
    for (int fahrenheit=0; fahrenheit<=20; fahrenheit++)
    {
        cout << fixed << setprecision(2) <<
        fahrenheit << "\t\t\t" << celcius(fahrenheit) << endl;
    }
    return 0;
}
float celcius(int fahrenheit)
{
    return 
        (static_cast<float>(5)/9)*(fahrenheit-32);
    
}
#include <iostream>
using namespace std;

int remainingTime(int, int);
int main()
{
    int age;
    cout << "Please enter age: ";
    cin >> age;
    cout << "You are " << age << " years old.";
    
    int desiredAge;
    cout << "How old do you wanna be when you die? ";
    cin >> desiredAge;
    
    cout << "You have " << remainingTime(age, desiredAge) << " years left. Have fun with them.";
    return 0;
}
int remainingTime(int age, int desiredAge)
{
    return desiredAge-age;
}
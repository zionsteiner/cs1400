#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
    
class StockTrader {
    
    private:
        float buyPrice=0.0;
        float profit=0.0;
        float avg=0.0;
        vector<int> prices;
        string ticker;
        
    public:
        StockTrader(string ticker) {
            ifstream fin("Data/"+ticker+".csv");
            float prc;
            while(fin>>prc)
                prices.push_back(prc);
        }
        
        void trade() {
            for(int i=0;i<prices.size();i++) {
                if(i>5) {
                    avg=(prices[i-1]+prices[i-2]+prices[i-3]+prices[i-4]+prices[i-5])/5.0;
                    
                    if(prices[i]>avg and buyPrice==0.0) { //buy
                        buyPrice=prices[i];
                        cout << "We bought at price: " << prices[i] << endl;
                    }
                    else if(prices[i]<avg and buyPrice!=0.0) { //sell
                        profit+=(prices[i]-buyPrice);
                        cout << "We sold at price: " << prices[i] << endl;
                        cout << "Profit for this trade: " << prices[i]-buyPrice << endl;
                        buyPrice=0.0;
                    }
                }
            }//for 
        }//trade
        
        void showProfit() {
            cout << "Profit was: " << profit << endl;
            cout << "Returns: " << profit/prices[0] << endl;
        }
};


int main() {
    StockTrader t1("goog");
    t1.trade();
    t1.showProfit();
}
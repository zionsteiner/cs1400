/*Zion Steiner
A02245003
WS7
Classy Dog Name: Markus
*/

#include <string>
#include <iostream>

using namespace std;

class Dog{
    private:
        int classiness;
        bool hasTopHat;
        string name;
    public:
        string getName();
        bool isClassy();
        Dog();
        Dog(int classiness, bool hasTopHat, string dogName);
};
// Definitions for all public member functions
string Dog::getName(){
    return name;
}
bool Dog::isClassy(){
if(hasTopHat)
    classiness*=2;
if(classiness>10)
    return true;
else
    return false;
}
Dog::Dog(){
    classiness=0;
    hasTopHat=false;
    name="Fido";
}
Dog::Dog(int newClassiness, bool newHasTopHat, string newName){
    classiness=newClassiness;
    hasTopHat=newHasTopHat;
    name=newName;
}


int main(){
    cout<<"Welcome to the most awaited for event of all year, the dog classiness contest!"<<endl;
    
    Dog loser;
    
    cout<<"Our first contestant, "<<loser.getName()<<endl;
    
    if(loser.isClassy()){
        cout<<"Unbelievable! How is this possible?"<<endl;
    }
    else{
        cout<<"Yeah, kind of expected that"<<endl;
    }
    Dog winner(500,true,"Ruffles the Second");
    
    cout<<"And now for our second contestant, give it up for "<<winner.getName()<<"!"<<endl;
    
    if(winner.isClassy()){
        cout<<"Incredible! "<<winner.getName()<<" does it again!"<<endl;
    }
    else{
        cout<<"Today is a dark, dark day for dog classiness contests"<<endl;
    }
    return 0;
}
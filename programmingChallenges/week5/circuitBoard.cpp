#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float boardPrice = 14.95, percentProfit = 0.35;
    float retailPrice = 14.95*(1+percentProfit);
    cout << "The retail price of the circuit board will be $" << setprecision(2)
         << fixed << retailPrice << " with a 35% margin." << endl;
    return 0;
}
#include <iostream>
using namespace std;

int main()
{
    int customers = 16500;
    float percentWeeklyConsumers = 0.15, percentPreferCitrus = 0.58;
    cout << "Approximately " << static_cast<int>(percentWeeklyConsumers*customers)
         << " customers drink one or more energy drinks per week." << endl;
    cout << "Of the customers who purchase one or energy drinks per week, "
         << static_cast<int>(customers*percentWeeklyConsumers*percentPreferCitrus) << " prefer citrus flavor."
         << endl;
    return 0;
}
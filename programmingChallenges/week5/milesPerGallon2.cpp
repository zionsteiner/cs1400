#include <iostream>
using namespace std;

int main()
{
    const int gallons=15, milesOnTank=375;
    float mpg=static_cast<float>(milesOnTank)/gallons;
    cout << "This car gets: " << mpg << " MPG." << endl;
}
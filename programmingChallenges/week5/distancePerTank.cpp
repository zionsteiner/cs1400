#include <iostream>
using namespace std;

int main()
{
    int tankCap = 20;
    float cityMPG = 23.5, highwayMPG = 28.9;
    float cityDistance = tankCap*cityMPG;
    float highwayDistance = tankCap*highwayMPG;
    cout << "A vehicle with a " << tankCap << " gallon tank that gets\n"
         << cityMPG << " MPG in the city and " << highwayMPG << " MPG on the highway\n"
         << "can travel " << cityDistance << " miles in the city with a full tank and\n"
         << highwayDistance << " miles on the highway." << endl;
    return 0;
}
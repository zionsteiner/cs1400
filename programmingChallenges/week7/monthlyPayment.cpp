#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double payment(double monthlyRate, int N, double L);

int main()
{
    double annualRate, L;
    int N;
    cout<<"What is the annual interest rate?"<<endl;
    cin>>annualRate;
    double monthlyRate=annualRate/12;
    cout<<"What is the amount on the loan?"<<endl;
    cin>>L;

cout<<"How many payments will be made on the loan?"<<endl;
cin>>N;
    cout<<"Loan Amount:"<<setw(15)<<"$"<<fixed<<showpoint<<setprecision(2)<<L<<endl;
    cout<<"Monthly Interest Rate:"<<setw(9)<<monthlyRate<<"%"<<endl;
    cout<<"Annual Interest Rate:"<<setw(10)<<annualRate<<"%"<<endl;
    cout<<"Number of payments:"<<setw(9)<<N<<endl;
    cout<<"Monthly payment:"<<setw(11)<<"$"<<setw(8)<<payment(monthlyRate, N, L)<<endl;
    cout<<"Amount paid back:"<<setw(10)<<"$"<<setw(8)<<payment(monthlyRate, N, L)*N<<endl;
    cout<<"Interest paid:"<<setw(13)<<"$"<<setw(8)<<payment(monthlyRate, N, L)*N-L<<endl;
    
    return 0;
}

double payment(double monthlyRate, int N, double L)
{
    double rate=monthlyRate/100;
    double mult=pow((1+rate), N);
    return (((rate*mult)/(mult-1))*L);
}
/*My favorite bird is the Hoopoe
Zion Steiner
A02245003*/
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

float compound(float principal, float rate, int times);
int main()
{
    float principal, rate;
    int times;
    
    cout<<"How much money would you like to use as principal?"<<endl;
    cin>>principal;
    cout<<"How many years will the money be left in the account?"<<endl;
    cin>>times;
    cout<<"The following table displays the amount of money in the account after the amount of time\n"
        <<"entered for several interest rates.\n";
    for (int rate=0; rate<=10; rate++)
    {
        string header=to_string(rate)+"%";
        cout<<setw(10)<<header;
    }
    cout<<endl;
    for(int rate=0; rate<=10; rate++)
    {
        cout<<setw(9)<<fixed<<setprecision(2)<<compound(principal, rate, times)<<" ";
    }
    return 0;
}

float compound(float principal, float rate, int times)
{
    float mult=1+(rate/100);
    return principal*pow(mult, times);
}
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

int main()
{
    unsigned int seed=time(0);
    srand(seed);
    do
    {
    int num1=(rand()%(1000))+1;
    int num2=(rand()%(1000))+1;
    cout<<setw(4)<<num1<<endl;
    cout<<"+"<<setw(3)<<num2<<endl;
    cout<<"_______"<<endl;
    cin.get();
    cout<<num1+num2;
    cout<<endl;
    }
    while(cin.get());
    return 0;
}
#include <iostream>
using namespace std;

int menu();
int main()
{
    int choice=menu();
    if (choice==1)
    {
        float radius;
        cout<<"What is the radius of the circle?"<<endl;
        cin>>radius;
        cout<<"The area of circle with radius "<<radius<< " is "<<3.14159*radius*radius<<endl;
    }
    else if (choice==2)
    {
        float length, width;
        cout<<"What is the width of the rectangle?"<<endl;
        cin>>width;
        cout<<"What is the length of the rectangle?"<<endl;
        cin>>length;
        cout<<"The area of the rectangle is "<<width*length<<endl;
    }
    else if (choice==3)
    {
        float base, height;
        cout<<"How long is the base of the triangle?"<<endl;
        cin>>base;
        cout<<"What is the height of the triangle?"<<endl;
        cin>>height;
        cout<<"The area of the triangle is "<<base*height*0.5<<endl;
    }
    else return 0;
    return 0;
}

int menu()
{
    int choice;
    do
    {
    cout<<"Geometry Calculator"<<endl;
    cout<<"Please select an option by entering the corresponding number."<<endl;
    cout<<"\t1. Calculate the Area of a Circle"<<endl;
    cout<<"\t2. Calculate the Area of a Rectangle"<<endl;
    cout<<"\t3. Calculate the Area of a Triangle"<<endl;
    cout<<"\t4. Quit"<<endl;
    cin>>choice;
    if (choice<1||choice>4)
        cout<<"Please enter a valid number."<<endl;
    }
    while (choice<1||choice>4);
    return choice;
}
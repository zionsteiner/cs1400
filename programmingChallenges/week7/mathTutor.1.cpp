#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

int main()
{
    unsigned int seed=time(0);
    int answer;
    srand(seed);
    do
    {
    int num1=(rand()%(1000))+1;
    int num2=(rand()%(1000))+1;
    cout<<setw(4)<<num1<<endl;
    cout<<"+"<<setw(3)<<num2<<endl;
    cout<<"_______"<<endl;
    cout<<"Answer: ";
    cin>>answer;
    if (answer==(num1+num2))
        cout<<"Congratulations! That was the right answer!"<<endl;
    else
        cout<<"Incorrect. The correct answer is:" <<num1+num2<<endl;
    cin.get();
    cout<<endl;
    }
    while(cin.get());
    return 0;
}
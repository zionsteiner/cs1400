/*Zion Steiner
A02245003
Work session 6: Compound Redux
I would like to cover the concept of objects and classes. Even though we haven't
really talked about them in class yet, it'd make it easier to understand C++ wiki entries.*/

#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

void printMenu(){
    cout<<"Press b to calculate basic compound interest, press a for advanced compounding. Press q to quit"<<endl;
}
float getComp(float principal, int years, float interestRate){
    float multiplier = 1+(interestRate/100);
    return (principal) * pow(multiplier, years);
}
float getAdvancedComp(float principal, int years, float interestRate, float annualInvestment){
    float accumulator=principal;
    float multiplier = 1+(interestRate/100);
    for(int i=1; i<=years; i++){
        accumulator=accumulator*multiplier+annualInvestment;
    }
    return accumulator;
}
void basicCompounding(){
    float principal, rate;
    int years;
    cout<<"Please input your principal investment: ";
    cin>>principal;
    cout<<"Please input your interest rate: ";
    cin>>rate;
    cout<<"Please input the number of years: ";
    cin>>years;
    
    cout<<"After "<<years<<" years, your investment of $"<<principal<<" has grown at a rate of "<<rate<<"% and reached a value of $"<<fixed<<setprecision(2)<<getComp(principal,years,rate)<<endl;
}

void advancedCompounding(){
    float principal, rate, annualInvestment;
    int years;
    cout<<"Please input your principal investment: ";
    cin>>principal;
    cout<<"Please input your interest rate: ";
    cin>>rate;
    cout<<"Please input the number of years: ";
    cin>>years;
    cout<<"Please enter your annual account contribution: ";
    cin>>annualInvestment;
    
    cout<<"After "<<years<<" years, your investment of $"<<principal<<" with an annual addition of $"<<annualInvestment
        <<" has grown at a rate of "<<rate<<"% and reached a value of $"<<fixed<<setprecision(2)<<getAdvancedComp(principal,years,rate,annualInvestment)<<endl;
}

int main(){
    bool done = false;
    cout<<"Welcome to your finance Advisor."<<endl;
    while(!done){
        printMenu();
        char input;
        cin>>input;
        switch(input){
            case 'b':
            case 'B': cout<<"Basic Compunding"<<endl;
                    basicCompounding();
                break;
            case 'a':
            case 'A': cout<<"Advanced Compounding"<<endl;
                    advancedCompounding();
                break;
            case 'q':
            case 'Q':cout<<"Fine, be that way"<<endl;
                     done = true;
                break;
            default: cout<<"Invalid input"<<endl;
        }
    }
}
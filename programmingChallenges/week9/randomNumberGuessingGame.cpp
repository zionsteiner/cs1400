#include <iostream>
#include <ctime>
using namespace std;

int main()
{
 int seed=time(0);
 srand(seed);
 int mysteryNum=rand()%20+1;
 cout<<"There exists a number between 1-20. Try to guess the number."<<endl;
 int guess;
 while(guess!=mysteryNum)
 {
     cout<<"What is your guess?"<<endl;
     cin>>guess;
     if(guess>mysteryNum)
        cout<<"Too high, try again."<<endl;
    else if(guess<mysteryNum)
        cout<<"Too low, try again."<<endl;
 }
 cout<<"GOOD job. You guessed it!"<<endl;
    return 0;
    
}
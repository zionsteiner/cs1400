#include <iostream>
using namespace std;

int main()
{
    //calories per minute
    float cpm=3.6;
    cout<<"This program tells you how many calories you will have burned on this\n"
        <<"treadmill after every 5 minutes of running."<<endl;
    for(int i=1; i<=6; i++)
    {
        cout<<"Calories burned after "<<5*i<<" minutes: "<<5*i*cpm<<endl;
    }
    return 0;
}
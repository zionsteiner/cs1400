/*
zion steiner
a02245003
cs1400 section 001
*/
//program to display text on screen

//tells preprocessor what files to add
#include <iostream>

//tells program what namespace to access
using namespace std;

//function to display text
int main()
{
    cout << "hello, world!" <<endl;
}